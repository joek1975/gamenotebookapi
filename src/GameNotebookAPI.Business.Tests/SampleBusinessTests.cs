using System;
using FluentAssertions;
using Xunit;

namespace GameNotebookAPI.Business.Tests
{
  public class SampleBusinessTests
  {
    [Fact]
    public void Example_IsTrue()
    {
      var sut = new Example();

      sut.IsTrue()
        .Should().BeTrue();
    }
  }
}
